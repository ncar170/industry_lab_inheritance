package ictgradschool.industry.lab_inheritance.ex2;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        Bird bird = new Bird();
        Dog dog = new Dog();
        Horse horse = new Horse();
        animals[0] = bird;
        animals[1] = dog;
        animals[2] = horse;

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        // TODO If the animal also implements IFamous, print out that corresponding info too.

        for (int i = 0; i < list.length; i++) {
            System.out.println(list[i].myName() + " says " + list[i].sayHello());
            if (list[i].isMammal()) {
                System.out.println(list[i].myName() + " is a mammal.");
            } else {
                System.out.println(list[i].myName() + " is a non-mammal.");
            }
            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs.");

            if (list[i] instanceof IFamous) {
                IFamous other = (IFamous) list[i];
                System.out.println("This is a name of a famous type of my animal: " + other.famous());
            }
            System.out.println("-------------------------------------------------------------------");
        }
    }

    public static void main(String[] args) {
        new ExerciseTwo().start();
    }
}
