public class Test1 extends Superclass {

    static int x = 15;
    static int y = 15;
    int x2 = 20;
    static  int y2 =20;

    Test1() {
        x2 = y2++;
    }

    public int foo2() {
        return x2;
    }

    public static int goo2() {
        return y2;
    }

    public static int goo() {
        return y2;
    }

    public static void main(String[] args) {
        Superclass s1 = new Superclass();
        System.out.println("S1.x = " + s1.x);
        System.out.println("S1.y = " + s1.y);
        System.out.println("S1.foo = " + s1.foo());
        System.out.println("S1.goo = " + s1.goo());
    }

}
